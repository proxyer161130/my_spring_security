-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: my_spring_security
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_authority`
--

DROP TABLE IF EXISTS `sys_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authority_name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '权限名字',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority_name_UNIQUE` (`authority_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='权限表，一个权限对应多个角色，一个角色也可以有多个权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_authority`
--

LOCK TABLES `sys_authority` WRITE;
/*!40000 ALTER TABLE `sys_authority` DISABLE KEYS */;
INSERT INTO `sys_authority` VALUES (1,'个人中心权限','2021-03-20 18:22:31'),(2,'用户管理权限','2021-03-20 18:22:31'),(3,'权限管理权限','2021-03-20 18:22:31'),(4,'超级管理员权限','2021-03-21 18:14:17');
/*!40000 ALTER TABLE `sys_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_authority_resource`
--

DROP TABLE IF EXISTS `sys_authority_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_authority_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authority_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='权限名拥有的资源，一个权限可以拥有多个资源，一个资源也可以被分配给多个权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_authority_resource`
--

LOCK TABLES `sys_authority_resource` WRITE;
/*!40000 ALTER TABLE `sys_authority_resource` DISABLE KEYS */;
INSERT INTO `sys_authority_resource` VALUES (1,1,1),(2,1,3),(3,2,2),(5,2,4),(6,2,5),(7,2,6),(15,3,7),(16,3,8),(17,3,9),(18,3,10),(19,3,11),(20,4,1),(21,4,2),(22,4,3),(23,4,4),(24,4,5),(25,4,6),(26,4,7),(27,4,8),(28,4,9),(29,4,10),(30,4,11);
/*!40000 ALTER TABLE `sys_authority_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_resource`
--

DROP TABLE IF EXISTS `sys_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL COMMENT '资源对应的父级id，null表示独立的',
  `resource_name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '一个资源对应的URL',
  `type` int(11) NOT NULL COMMENT '0-父级菜单，1-子级菜单，2-按钮',
  `sort` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '资源排序，根据ASCII表，选择0-9，A-Z组合，不用a-z，以A-Z开头，每个菜单占据两位，先按照父级菜单排序，再根据子菜单排序。\\n例如：\\n第一个菜单：一级菜单A0，二级菜单A000，三级A00000\\n第二个菜单：一级菜单B0，二级菜单B000，三级B00000\\n……\\n第二十七个菜单：\\n一级菜单A1，二级菜单A100，三级A10000\\n',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource_name_UNIQUE` (`resource_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='资源表，一个权限对应多个资源，一个资源也可以对应多个权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_resource`
--

LOCK TABLES `sys_resource` WRITE;
/*!40000 ALTER TABLE `sys_resource` DISABLE KEYS */;
INSERT INTO `sys_resource` VALUES (1,NULL,'用户',NULL,0,'A0','2021-03-20 18:10:06'),(2,1,'用户管理','/user/manage',1,'A001','2021-03-20 18:17:47'),(3,1,'个人中心','/user/profile',1,'A000','2021-03-20 18:10:06'),(4,2,'用户添加','/user/add',2,'A00100','2021-03-20 18:17:47'),(5,2,'用户编辑','/user/edit',2,'A00101','2021-03-20 18:17:47'),(6,2,'用户删除','/user/delete',2,'A00102','2021-03-20 18:17:47'),(7,NULL,'权限',NULL,0,'B0','2021-03-20 18:19:20'),(8,7,'权限管理','/authority/manage',1,'B000','2021-03-20 18:20:01'),(9,8,'权限添加','/authority/add',2,'B00000','2021-03-20 18:21:41'),(10,8,'权限编辑','/authority/edit',2,'B00001','2021-03-20 18:21:41'),(11,8,'权限删除','/authority/delete',2,'B00002','2021-03-20 18:21:41');
/*!40000 ALTER TABLE `sys_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '角色名字',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name_UNIQUE` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色表，一个角色对应多个用户，一个用户对应多个角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'普通用户','2021-03-20 18:23:08'),(2,'普通管理员','2021-03-20 18:23:08'),(3,'超级管理员','2021-03-20 18:23:08');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_authority`
--

DROP TABLE IF EXISTS `sys_role_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `authority_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='根据权限名字分配权限给角色，一个权限可以被分配给多个角色，一个角色也可以拥有多个权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_authority`
--

LOCK TABLES `sys_role_authority` WRITE;
/*!40000 ALTER TABLE `sys_role_authority` DISABLE KEYS */;
INSERT INTO `sys_role_authority` VALUES (1,1,1),(2,2,1),(3,2,2),(4,3,1),(5,3,2),(6,3,3);
/*!40000 ALTER TABLE `sys_role_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '用户密码，使用md5摘要',
  `username` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '登陆使用的用户名',
  `nickname` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称，主页显示',
  `enable` int(11) NOT NULL DEFAULT '0' COMMENT '用户账户是否启用，1-启用，0-禁用，默认0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '用户信息更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nickname_UNIQUE` (`nickname`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'$2a$10$FNkDIGRFVWqbddL6kew.8uT8GW3oHRCGsCd/4d1r0HLvU1X8aYf96','user1','user1',0,'2021-03-18 15:39:54',NULL),(2,'$2a$10$6c/UBXqB2n8WG0CMhDpXR.DoEz8Y.YNWqnAQuYgjQvxsUiY5GSgqW','user2','user2',1,'2021-03-18 15:39:54',NULL),(3,'$2a$10$mQ1Y28b8fr70CAE0WtrFuuIqMNsxWJhU6E9P7UfrHhVSe7QDNQhTy','admin1','admin1',1,'2021-03-18 15:39:54',NULL),(4,'$2a$10$zCvD5tcDpmKwMedleFXTyeC4F9ScuoXLv9aDgsLFujRMlPfH185ey','root1','root1',1,'2021-03-18 15:39:54',NULL);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户-角色，一个用户对应多个角色，一个角色对应多个用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1,1),(2,2,1),(3,3,2),(4,4,3);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'my_spring_security'
--

--
-- Dumping routines for database 'my_spring_security'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-22 22:51:55
