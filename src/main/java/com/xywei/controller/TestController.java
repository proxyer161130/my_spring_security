package com.xywei.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@PreAuthorize("hasAuthority('test')")
	@GetMapping("/test1")
	public String test1() {
		return "ok";
	}

	@GetMapping("/test2")
	public Map<String, String> test2() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "test ok!");
		return map;
	}

	@GetMapping("/test")
	public TestController test() {
		TestController testController = new TestController();
		testController.setMessage("ok! testController");
		return testController;
	}
	/**
	 * 管理员有root/test权限
	 *@Description 
	 *@Datetime 2021年3月20日 下午5:42:53<br/>
	 * @return
	 */
	@PreAuthorize("hasAuthority('root')")
	@GetMapping("/root1")
	public String root1() {
		return "ok!root1";
	}


	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
