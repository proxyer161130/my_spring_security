package com.xywei.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	@GetMapping("/profile")
	public String userProfile() {
		return "user profile page";
	}

	@GetMapping("/add")
	public String userAdd() {
		return "user add page";
	}

	@GetMapping("/delete")
	public String userDelete() {
		return "user delete page";
	}

	@GetMapping("/edit")
	public String userEdit() {
		return "user edit page";
	}

}
