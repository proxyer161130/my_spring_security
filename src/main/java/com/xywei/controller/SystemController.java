package com.xywei.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SystemController {
	
	/**
	 * 
	 *@Description 有资源才会自动跳到template，否则跳到static
	 *@Datetime 2021年3月20日 下午2:26:39<br/>
	 * @return
	 */
	@RequestMapping(value = { "/", "/index" })
	public String homePage() {
		return "/index";
	}

	@RequestMapping("/userLoginPage")
	public String loginPage() {
		return "/login";
	}

	@RequestMapping("/loginFailure")
	public String loginFailure() {
		return "/loginFailure";
	}
}
