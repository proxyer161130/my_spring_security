package com.xywei.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authority")
public class AuthorityController {

	@GetMapping("/add")
	public String authorityAdd() {
		return "authority page add!";
	}

	@GetMapping("/delete")
	public String authorityDelete() {
		return "authority page delete!";
	}

	@GetMapping("/edit")
	public String authorityEdit() {
		return "authority page edit!";
	}
}
