package com.xywei.config.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.access.intercept.InterceptorStatusToken;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author future
 * @Datetime 2021年3月21日 下午12:19:25<br/>
 * @Description 作用：<br>
 *              1.自定义初始化权限数据 <br>
 *              2.自定义认证管理器<br>
 *              3.自定义决策管理器
 */
//@Component("securityCustomFilterInterceptor")
public class SecurityCustomFilterInterceptor extends AbstractSecurityInterceptor implements Filter{

	private FilterInvocationSecurityMetadataSource securityMetadataSource;
	
	public FilterInvocationSecurityMetadataSource getSecurityMetadataSource() {
		return securityMetadataSource;
	}

	public void setSecurityMetadataSource(FilterInvocationSecurityMetadataSource securityMetadataSource) {
		this.securityMetadataSource = securityMetadataSource;
	}
	
	/********************************************************************
	 * extends AbstractSecurityInterceptor start
	 ********************************************************************/
	//注入初始化的权限数据

	@Override
	public Class<?> getSecureObjectClass() {
		return FilterInvocation.class;
	}

	@Override
	public SecurityMetadataSource obtainSecurityMetadataSource() {
		return this.securityMetadataSource;
	}

	/********************************************************************
	 * extends AbstractSecurityInterceptor end
	 ********************************************************************/


	/********************************************************************
	 * implements Filter start
	 ********************************************************************/
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		FilterInvocation fi = new FilterInvocation(request, response, chain);
		//进入最后一道拦截器之前先进行权限检查
		invoke(fi);
	}

	private void invoke(FilterInvocation fi) throws IOException, ServletException{
		InterceptorStatusToken token = super.beforeInvocation(fi);
		try {
			fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
		} finally {
			super.afterInvocation(token, null);
		}
	}
	/********************************************************************
	 * implements Filter end
	 ********************************************************************/


}
