package com.xywei.config.security;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xywei.domain.Authority;
import com.xywei.service.AuthorityService;

@Component("securityService")
public class SecurityService {

	@Autowired
	private AuthorityService authorityService;

	public Set<Authority> getAuthoritiesWithResource() {
		Set<Authority> authoritiesWithResource = authorityService.getAuthoritiesWithResource();
		return authoritiesWithResource;
	}
}
