package com.xywei.domain;

import java.util.Date;
import java.util.Set;

import org.springframework.format.annotation.DateTimeFormat;

public class Authority {

	private Integer id;
	private String authorityName;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	private Set<Resource> resources;

	public Authority() {
	}

	public Authority(Integer id, String authorityName, Date createTime, Set<Resource> resources) {
		this.id = id;
		this.authorityName = authorityName;
		this.createTime = createTime;
		this.resources = resources;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	@Override
	public String toString() {
		return "Authority [id=" + id + ", authorityName=" + authorityName + ", createTime=" + createTime
				+ ", resources=" + resources + "]";
	}

}
