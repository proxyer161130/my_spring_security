package com.xywei.domain;

import java.util.Date;
import java.util.Set;

import org.springframework.format.annotation.DateTimeFormat;

public class Role {
	
	private Integer id;
	private String roleName;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	private Set<Authority> authorities;
	
	public Role() {
	}

	public Role(Integer id, String roleName, Date createTime, Set<Authority> authorities) {
		this.id = id;
		this.roleName = roleName;
		this.createTime = createTime;
		this.authorities = authorities;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", roleName=" + roleName + ", createTime=" + createTime + ", authorities="
				+ authorities + "]";
	}

}
