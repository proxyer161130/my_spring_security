package com.xywei.domain;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Resource {

	private Integer id;
	private Integer parentId;
	private String resourceName;
	private String url;
	/**
	 * 0-父级菜单，1-子级菜单，2-按钮
	 */
	private Integer type;
	/**
	 * 资源排序，根据ASCII表，选择0-9，A-Z组合，不用a-z，以A-Z开头，每个菜单占据两位，先按照父级菜单排序，再根据子菜单排序。 例如：<br>
	 * 第一个菜单：一级菜单A0，二级菜单A000，三级A00000 <br>
	 * 第二个菜单：一级菜单B0，二级菜单B000，三级B00000<br>
	 ......<br>
	 * 第二十七个菜单：一级菜单A1，二级菜单A100，三级A10000
	 */
	private String sort;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Resource() {
	}

	public Resource(Integer id, Integer parentId, String resourceName, String url, Integer type, String sort,
			Date createTime) {
		this.id = id;
		this.parentId = parentId;
		this.resourceName = resourceName;
		this.url = url;
		this.type = type;
		this.sort = sort;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "Resource [id=" + id + ", parentId=" + parentId + ", resourceName=" + resourceName + ", url=" + url
				+ ", type=" + type + ", sort=" + sort + ", createTime=" + createTime + "]";
	}

}
