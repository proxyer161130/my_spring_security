package com.xywei.dao;

import org.apache.ibatis.annotations.Mapper;

import com.xywei.domain.User;

@Mapper
public interface UserDao {
	
	User findByUsername(String username);
	
	User findUserAndRoleAndAuthorityByUsername(String username);

}
