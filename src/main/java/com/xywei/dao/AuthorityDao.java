package com.xywei.dao;

import java.util.Set;

import org.apache.ibatis.annotations.Mapper;

import com.xywei.domain.Authority;

@Mapper
public interface AuthorityDao {
	/**
	 * 
	 *@Description 查找权限，并且把权限下面配置的资源URL都查出来
	 *@Datetime 2021年3月21日 下午2:12:12<br/>
	 * @return
	 */
	Set<Authority> getAuthoritiesWithResource();
}
