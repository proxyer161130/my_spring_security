package com.xywei.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xywei.dao.AuthorityDao;
import com.xywei.domain.Authority;
import com.xywei.service.AuthorityService;

@Service("authorityService")
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	private AuthorityDao authorityDao;

	@Override
	public Set<Authority> getAuthoritiesWithResource() {
		Set<Authority> authoritiesWithResource = authorityDao.getAuthoritiesWithResource();
		return authoritiesWithResource;
	}

}
