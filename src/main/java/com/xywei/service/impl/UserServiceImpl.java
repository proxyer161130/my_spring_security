package com.xywei.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.xywei.dao.UserDao;
import com.xywei.domain.User;
import com.xywei.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User findByUsername(String username) {
		if (StringUtils.isEmpty(username)) {
			return null;
		}
		User user = userDao.findByUsername(username);
		return user;
	}

	@Override
	public User findUserAndRoleAndAuthorityByUsername(String username) {
		if (StringUtils.isEmpty(username)) {
			return null;
		}
		User user = userDao.findUserAndRoleAndAuthorityByUsername(username);
		return user;
	}

}
