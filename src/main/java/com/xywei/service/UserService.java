package com.xywei.service;

import com.xywei.domain.User;

public interface UserService {

	User findByUsername(String username);
	
	User findUserAndRoleAndAuthorityByUsername(String username);
}
