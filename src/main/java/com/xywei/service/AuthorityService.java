package com.xywei.service;

import java.util.Set;

import com.xywei.domain.Authority;

public interface AuthorityService {

	/**
	 * 
	 *@Description 查找权限，并且把权限下面配置的资源URL都查出来
	 *@Datetime 2021年3月21日 下午2:11:11<br/>
	 * @return
	 */
	Set<Authority> getAuthoritiesWithResource();
}
