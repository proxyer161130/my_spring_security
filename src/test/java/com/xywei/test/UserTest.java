package com.xywei.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xywei.domain.User;
import com.xywei.service.UserService;

public class UserTest extends MyTest {

	@Autowired
	private UserService userService;

	@Test
	public void testFindUserByUsername() {
		String username = "user1";
		User user = userService.findByUsername(username);
		assertNotNull(user, username + "not found!");
	}

	@Test
	public void testFindUserAndRoleAndAuthorityByUsername() {
		String username = "user1";
		User user = userService.findUserAndRoleAndAuthorityByUsername(username);
		System.out.println(user);
	}

}
