package com.xywei.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;

import com.xywei.domain.Authority;
import com.xywei.domain.Resource;
import com.xywei.service.AuthorityService;

public class AuthorityServiceTest extends MyTest {

	@Autowired
	private AuthorityService authorityService;

	@Test
	public void getAuthorityWithResource() {
		Set<Authority> authoritiesWithResource = authorityService.getAuthoritiesWithResource();
		assertNotNull(authoritiesWithResource, "authorties are null");
		System.out.println(authoritiesWithResource);

	}

	@Test
	public void initResourceConfigAuthorityTest() {

		// 格式: ["url"="['权限1'，'权限2']"]
		Map<String, Collection<ConfigAttribute>> resourceAuthority = new HashMap<String, Collection<ConfigAttribute>>();
		// 获取数据库的权限表所有数据，带URL资源
		Set<Authority> authoritiesWithResource = authorityService.getAuthoritiesWithResource();

		if (authoritiesWithResource != null) {
			// 一个资源, 对应多个权限
			resourceAuthority = obtainUrlWithAuthority(authoritiesWithResource);
		}
		System.out.println(resourceAuthority);

	}

	private Map<String, Collection<ConfigAttribute>> obtainUrlWithAuthority(Set<Authority> authoritiesWithResource) {

		Map<String, Collection<ConfigAttribute>> resourceAuthority = new HashMap<String, Collection<ConfigAttribute>>();
		// 权限的名字在设计的时候就决定了是唯一的
		for (Authority authority : authoritiesWithResource) {
			String authorityName = authority.getAuthorityName();
			Set<Resource> resources = authority.getResources();
			for (Resource resource : resources) {
				// 跳过父菜单目录
				if (resource.getParentId() == null) {
					continue;
				}
				String url = resource.getUrl();
				ConfigAttribute attribute = new SecurityConfig(authorityName);
				Collection<ConfigAttribute> attributes = new HashSet<ConfigAttribute>();
				if (resourceAuthority.containsKey(url)) {
					attributes = resourceAuthority.get(url);
				}
				attributes.add(attribute);
				resourceAuthority.put(url, attributes);
			}
		}

		return resourceAuthority;
	}

}
