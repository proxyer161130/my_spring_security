package com.xywei.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.sql.DataSource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DBTest extends MyTest {

	@Autowired
	private DataSource dataSource;

	@Test
	public void testDB() {
		assertNotNull(dataSource, "error!");
		System.out.println(dataSource == null ? null : dataSource.getClass());
	}

}
