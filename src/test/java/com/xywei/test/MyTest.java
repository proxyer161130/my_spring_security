package com.xywei.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.xywei.StartApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartApplication.class)
public class MyTest {

}
